﻿using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Models.AdminViewModels.CartViewModels;
using JamRockApplication_v1.Models.AdminViewModels.ProductViewModels;
using JamRockApplication_v1.Models.ShopViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Services;
using Microsoft.AspNetCore.Hosting;
using RazorEngine.Templating;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Shop
{
    public class ShopController : Controller
    {
        private readonly IRepository<Product> _productRepo;
        private readonly IRepository<Cart> _cartRepo;
        private readonly IRepository<Order> _orderRepo;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<ShopController> _logger;
        private readonly IEmailSender _msgService;
        private ISession Session => _httpContextAccessor.HttpContext.Session;
        public const string CartSessionKey = "Id";
        private readonly IHostingEnvironment _hostingEnvironment;

        public ShopController(IRepository<Product> productRepo,
         IRepositoryManager repoManager,
         IHttpContextAccessor httpContextAccessor,
         IRepository<Cart> cartRepo,
            IRepository<Customer> customerRepo,
            IRepository<Order> orderRepo,
        IEmailSender msgService,
         ILogger<ShopController> logger,
            IHostingEnvironment hostingEnvironment)
        {
            _productRepo = productRepo;
            _httpContextAccessor = httpContextAccessor;
            _cartRepo = cartRepo;
            _logger = logger;
            _msgService = msgService;
            _orderRepo = orderRepo;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var featuredProducts = await _productRepo.Findfeatured();
            var newProducts = await _productRepo.FindNewest();
            var randomProducts = await _productRepo.FindRandom();
            var topProducts = await _productRepo.FindTop();

            var shvm = new shopHomeViewModel();

            try
            {
                foreach (var p in newProducts)
                {
                    var model = new newProducts
                    {
                        ID = p.ID,
                        Name = p.Name,
                        productImage = p.productImageByteArray,
                        Price = p.Price
                    };


                    shvm.New.Add(model);
                }

                foreach (var p in randomProducts)
                {
                    var model = new randomProducts
                    {
                        ID = p.ID,
                        Name = p.Name,
                        productImage = p.productImageByteArray,
                        Price = p.Price
                    };


                    shvm.Random.Add(model);
                }

                foreach (var p in topProducts)
                {
                    var model = new topProducts
                    {
                        ID = p.ID,
                        Name = p.Name,
                        productImage = p.productImageByteArray,
                        Price = p.Price
                    };


                    shvm.Top.Add(model);
                }


                foreach (var product in featuredProducts)
                {
                    var productViewModel = new featuredProducts
                    {
                        ID = product.ID,
                        Name = product.Name,
                        Price = product.Price,
                        Quantity = product.Quantity,
                        Description = product.Description,
                        Category = product.Category,
                        productImage = product.productImageByteArray,
                        SoldQuantity = product.QuantitySold,
                        remainingQuantity = product.Quantity - product.QuantitySold,
                        ColorSizes = product.ColorSizes.Where(cs => cs.QuantityLeft > 1).ToList()

                    };

                    shvm.Featured.Add(productViewModel);
                }
            }
            catch (Exception e)
            {
               _logger.LogError("Error Setting up Shop Index Page:"+e);
            }
          

            return View(shvm);
        }

        public async Task<IActionResult> Category(string category)
        {
     
            if (string.IsNullOrEmpty(category))
            {
                return RedirectToAction("Index");
            }
            try
            {
                var response = await _productRepo.GetAllInCategory(category);

                var productsInCategory = new List<DisplayProductViewModel>();

                foreach(var product in response)
                {
                    var productViewModel = new DisplayProductViewModel
                    {
                        ID = product.ID,
                        Name = product.Name,
                        Price = product.Price,
                        Quantity = product.Quantity,
                        Description = product.Description,
                        Category = product.Category,
                        productImage = product.productImageByteArray,
                        SoldQuantity = product.QuantitySold,
                        remainingQuantity = product.Quantity - product.QuantitySold
                    };

                    productsInCategory.Add(productViewModel);
                }

                return View(productsInCategory);

            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            try
            {
                var product = await _productRepo.FindById(id);

                var csAvaliable = product.ColorSizes.Where(cs => cs.QuantityLeft > 1).ToList();

                var productViewModel = new DisplayProductViewModel
                {
                    ID = product.ID,
                    Name = product.Name,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    Description = product.Description,
                    Category = product.Category,
                    productImage = product.productImageByteArray,
                    SoldQuantity = product.QuantitySold,
                    remainingQuantity = product.Quantity - product.QuantitySold,
                    ColorSizes = csAvaliable
                };




                return View(productViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Cart()
        {
            try
            {
                //first check if this session has a session key
                if (_httpContextAccessor.HttpContext.Session.Get(CartSessionKey) == null)
                {
                    //if user is logged in, use username as session key
                    if (!string.IsNullOrWhiteSpace(_httpContextAccessor.HttpContext.User.Identity.Name))
                    {
                        Session.SetString(CartSessionKey, _httpContextAccessor.HttpContext.User.Identity.Name);
                    }
                    else //use a new random guid as the session key
                    {
                        var tempCartId = Guid.NewGuid();
                        Session.SetString(CartSessionKey, tempCartId.ToString());
                    }
                }
                //done checking if session has a session key, and setting one if one does not exist

                var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));
                var displayCart = new CartViewModel();

                if (cart == null)
                {

                    return View(displayCart);
                }
                displayCart.Quantity = TotalQuantity(cart.cartItems);
                displayCart.totalCost = CartCost(cart.cartItems);

                foreach (var t in cart.cartItems)
                {
                    t.Amount = t.ProductQuantity * t.ProductPrice;
                    displayCart.cartItems.Add(t);
                }

                ViewBag.Amount = displayCart.totalCost * 100;
                ViewBag.reference = Guid.NewGuid().ToString();
                ViewBag.key = "pk_live_7d721ad5edba7f71940b5658be9be93a5f549a02";

                return View(displayCart);
            }
            catch (Exception e)
            {
                _logger.LogError("Error with cart"+e);
                return View(null);
            }
            //await CartTable();
            //return View();
        }

        [HttpGet]
        public async Task<IActionResult> CartTable()
        {
            //first check if this session has a session key
            if (_httpContextAccessor.HttpContext.Session.Get(CartSessionKey) == null)
            {
                //if user is logged in, use username as session key
                if (!string.IsNullOrWhiteSpace(_httpContextAccessor.HttpContext.User.Identity.Name))
                {
                    Session.SetString(CartSessionKey, _httpContextAccessor.HttpContext.User.Identity.Name);
                }
                else //use a new random guid as the session key
                {
                    var tempCartId = Guid.NewGuid();
                    Session.SetString(CartSessionKey, tempCartId.ToString());
                }
            }
            //done checking if session has a session key, and setting one if one does not exist

            var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));
            var displayCart = new CartViewModel();

            if (cart == null)
            {

                return PartialView(displayCart);
            }
            displayCart.Quantity = TotalQuantity(cart.cartItems);
            displayCart.totalCost = CartCost(cart.cartItems);

            foreach (var t in cart.cartItems)
            {
                t.Amount = t.ProductQuantity * t.ProductPrice;
                displayCart.cartItems.Add(t);
            }

            ViewBag.Amount = displayCart.totalCost * 100;
            ViewBag.reference = Guid.NewGuid().ToString();
            ViewBag.key = "pk_test_2172670a6c45fd0db2c3f8bbe0d59cf9e8f6d2fd";

            return PartialView(displayCart);
        }

        public async Task<IActionResult> AddToCart(int id,int? choice)
        {
            try
            {
                var selectedProduct = await _productRepo.FindById(id);


                var cs = selectedProduct.ColorSizes.Find(c => c.Id.Equals(choice));


                if (cs == null)
                {
                    return BadRequest(new { error = "Click Product Name And Select Size" });
                }

                if (cs.QuantityLeft < 1)
                {
                    return BadRequest(new { error = "Out of Stock"});
                }

                //first check if this session has a session key
                if (_httpContextAccessor.HttpContext.Session.Get(CartSessionKey) == null)
                {
                    //if user is logged in, use username as session key
                    if (!string.IsNullOrWhiteSpace(_httpContextAccessor.HttpContext.User.Identity.Name))
                    {
                        Session.SetString(CartSessionKey, _httpContextAccessor.HttpContext.User.Identity.Name);
                    }
                    else //use a new random guid as the session key
                    {
                        var tempCartId = Guid.NewGuid();
                        Session.SetString(CartSessionKey, tempCartId.ToString());
                    }
                }
                //done checking if session has a session key, and setting one if one does not exist

                //check here is a cart already exists with the session key 
                var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));

                //if no cart exists with the current session key, create a new cart and give it the session key as its ID
                if (cart == null)
                {

                    var newCart = new Cart {Id = Session.GetString(CartSessionKey)};

                    var item = new cartItem
                    {
                        ProductSku = selectedProduct.ID,
                        ProductName = selectedProduct.Name,
                        ProductPrice = selectedProduct.Price,
                        ProductQuantity = 1,
                        ProductSize =  cs.Size,
                        ProductColor = cs.Color,
                        ProductImage = selectedProduct.productImageByteArray
                    };


                    item.Amount = item.ProductQuantity * item.ProductPrice;

                    newCart.cartItems.Add(item);

                    await _cartRepo.Add(newCart);
                }
                else //use the returned cart as the cart for this session
                {
                    var productInList = cart.cartItems.Find(x => x.ProductSku.Equals(id) && x.ProductColor.Equals(cs.Color) && x.ProductSize.Equals(cs.Size));

                    if (productInList != null)
                    {
                        productInList.ProductQuantity++;
                    }
                    else
                    {
                        var item = new cartItem
                        {
                            ProductSku = selectedProduct.ID,
                            ProductName = selectedProduct.Name,
                            ProductPrice = selectedProduct.Price,
                            ProductQuantity = 1,
                            ProductImage = selectedProduct.productImageByteArray,
                            ProductSize = cs.Size,
                            ProductColor = cs.Color
                        };

                        item.Amount = item.ProductQuantity * item.ProductPrice;

                        cart.cartItems.Add(item);
                    }

                    await _cartRepo.Update(cart);
                }

                return Ok(new { result = "OK" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception in adding to cart" + ex);
                return BadRequest(new { error = "error" });
            }
        }

        public async Task<IActionResult> RemoveFromCart(int id)
        {
            var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));
            try
            {
                var cartIteminList = cart.cartItems.Find(x => x.Id.Equals(id));

                cart.cartItems.Remove(cartIteminList);

                await _cartRepo.Update(cart);
                return Ok(new { result = "OK" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception in adding to cart" + ex);
                return BadRequest(new { error = "error" });
            }

        }

        public async Task<IActionResult> UpdateItemQuantityPlus(int id)
        {
            var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));

            try
            {
                var productInList = cart.cartItems.Find(x => x.Id.Equals(id));

                var productinDb = await _productRepo.FindById(productInList.ProductSku);

                var cs = productinDb.ColorSizes.Find(a => a.Color.Equals(productInList.ProductColor) &&
                                                          a.Size.Equals(productInList.ProductSize));

                if (cs.QuantityLeft < 1)
                {
                    return BadRequest(new { error = "sorry,out of stock. you got the last one." });
                }

                productInList.ProductQuantity++;

                await _cartRepo.Update(cart);

                return Ok(new { result = "OK" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception in adding to cart" + ex);
                return BadRequest(new { error = "error" });
            }
        }

        public async Task<IActionResult> UpdateItemQuantityMinus(int id)
        {
            var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));

            try
            {
                var productInList = cart.cartItems.Find(x => x.Id.Equals(id));

                productInList.ProductQuantity--;

                await _cartRepo.Update(cart);
                return Ok(new { result = "OK" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception in adding to cart" + ex);
                return BadRequest(new { error = "error" });
            }

        }

        public double CartCost(List<cartItem> items)
        {
            double total = 0;

            if (items != null)
            {

                foreach (var t in items)
                {
                    total = total + (t.ProductPrice * t.ProductQuantity);
                }

            }

            return total;
        }

        public int TotalQuantity(List<cartItem> items)
        {
            var total = 0;

            if (items != null)
            {

                foreach (var t in items)
                {
                    total = total + t.ProductQuantity;
                }

            }

            return total;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductSoldQuantity()
        {
            var cart = await _cartRepo.FindById(Session.GetString(CartSessionKey));

            try
            {
                foreach (var item in cart.cartItems)
                {
                    var product = await _productRepo.FindById(item.ProductSku);

                    if (product == null)
                    {
                        return BadRequest(new { error = "Error" });
                    }
                    var pf = product.ColorSizes.Find(c => c.Color.Equals(item.ProductColor) && c.Size.Equals(item.ProductSize));

                    pf.QuantityLeft -= item.ProductQuantity;

                    await _productRepo.Update(product);
                }

                cart.cartItems.Clear();

               await _cartRepo.Update(cart);

               return Ok(new {result="ok" });          
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest(new {error="Error" });
            }

        }

        public async Task<IActionResult> TranscationComplete(ptransactionVmcs model)
        {
            var errroId = 0;
            var errorPaymentref = string.Empty;
            var templateFolderPath = Path.Combine(_hostingEnvironment.ContentRootPath, "Views/EmailTemplates/Email.cshtml");
            var templateService = new TemplateService();
            try
            {
           
                //foreach ticket item, create a new booking
                var c = await _cartRepo.FindById(Session.GetString(CartSessionKey));

                var newOrder = new Order
                {                                                  
                    Customer = new Customer
                    {
                        Name = model.name,
                        Address = model.address,
                        Email = model.email,
                        Phone = model.phone
                    },
                    DeliveryType = model.DeliveryMode
                };

                foreach (var t in c.cartItems)
                {                  
                  newOrder.OrderItems.Add(t);

                    var product = await _productRepo.FindById(t.ProductSku);

                    if (product == null)
                    {
                        return BadRequest(new { error = "Error" });
                    }
                    var pf = product.ColorSizes.Find(cd => cd.Color.Equals(t.ProductColor) && cd.Size.Equals(t.ProductSize));

                    pf.QuantityLeft -= t.ProductQuantity;

                    if (pf.QuantityLeft < 0.25 * pf.Quantity)
                    {
                        await _msgService.SendEmailSupportAsync("", "Shop Alert", "Please be informed that product: " + product.Name +" ,has less than 25% of its original pieces left in inventory. Re-stock as soon as possible.");
                    }
                    if (pf.QuantityLeft == 0)
                    {
                        await _msgService.SendEmailSupportAsync("", "Shop Alert", "Please be informed that product: " + product.Name + " ,has run out of stock in inventory. Re-stock as soon as possible.");
                    }

                    await _productRepo.Update(product);
                }

                await _orderRepo.Add(newOrder);

                errroId = newOrder.Id;
                errorPaymentref = model.refNumber;
                c.cartItems.Clear();

                await _cartRepo.Update(c);

                try
                {                        
                    var emailHtmlBody = templateService.Parse(System.IO.File.ReadAllText(templateFolderPath), newOrder, null, null);
                    await _msgService.SendEmailAsync(model.email, "Order For Jamrock", emailHtmlBody);
                    await _msgService.SendEmailSupportAsync("", "New Order Alert", "A New Order Has Been Placed with id: " + newOrder.Id);

                }
                catch (Exception ex )
                {
                    _logger.LogError("Error Sending order receipt for order:" +newOrder.Id, ex.ToString());
                    await _msgService.SendEmailSupportAsync("", "Order Error Alert", "There was an sending email for order id " + errroId + ". Payment Reference number is: " + errorPaymentref + ". Customer Name is: " + model.name + ", Email is:" + model.email + ", Phone number is: " + model.phone);
                }


                return Ok(new { result = "ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error doing post transaction", ex.ToString());
                await _msgService.SendEmailSupportAsync("", "Order Error Alert", "There was an error completing a sale with order id "+ errroId +". Payment Reference number is: "+errorPaymentref+". Customer Name is: "+ model.name +", Email is:"+model.email+", Phone number is: "+model.phone);
                return BadRequest(new { Error = "error" });
            }
        }

    }
}
