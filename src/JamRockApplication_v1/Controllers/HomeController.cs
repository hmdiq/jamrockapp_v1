﻿using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Models.AdminViewModels.EventViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using JamRockApplication_v1.Services;
using JamRockApplication_v1.Models;

namespace JamRockApplication_v1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<Rave> _eventRepo;
        private readonly ILogger<HomeController> _logger;
        private readonly IEmailSender _msgService;

        public HomeController(IRepositoryManager repoManager, IHostingEnvironment environment, IRepository<Rave> eventRepo, ILogger<HomeController> logger, IEmailSender msgService)
        {
            _eventRepo = eventRepo;
            _logger = logger;
            _msgService = msgService;
        }


        public async Task<IActionResult> Index()
        {
            var hpvm = new homePageViewModels();

            try
            {
                var latestEvent = await _eventRepo.FindLatest();
                var upcoming = await _eventRepo.FindNewest();
                var previous = await _eventRepo.FindTop();


                if(latestEvent != null)
                {
                    hpvm.latestModel = new DisplayEventViewModel
                    {
                        ID = latestEvent.ID,
                        Name = latestEvent.Name,
                        eventDate = latestEvent.eventDate,
                        eventImage = latestEvent.eventImage,
                        Location = latestEvent.Location
                    };

                }


                foreach (var item in upcoming)
                {
                    var model = new DisplayEventViewModel
                    {
                        ID = item.ID,
                        Name = item.Name,
                        eventImage = item.eventImage,
                        eventDate = item.eventDate,
                        Description = item.Description,
                        Location = item.Location
                    };


                    hpvm.upComingEvents.Add(model);

                }


                foreach (var item in previous)
                {
                    var model = new DisplayEventViewModel
                    {
                        ID = item.ID,
                        Name = item.Name,
                        eventImage = item.eventImage,
                        eventDate = item.eventDate,
                        Description = item.Description,
                        Location = item.Location
                    };


                    hpvm.previousEvents.Add(model);
                }

                return View(hpvm);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return View(hpvm);
            }


       
        }

        public async Task<IActionResult> Festivals()
        {
            var fvm = new FestivalViewModel();
            try
            {
                var result = await _eventRepo.GetAll();  

                foreach (var rave in result)
                {
                    var eventViewModel = new DisplayEventViewModel
                    {
                        ID = rave.ID,
                        Name = rave.Name,
                        Description = rave.Description,
                        eventImage = rave.eventImage,
                        Location = rave.Location,
                        eventDate = rave.eventDate
                    };


                    fvm.events.Add(eventViewModel);
                };

                return View(fvm);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return View(fvm);
            }
        }

        public async Task<IActionResult> details(int? id)
        {

            if (id == null)
            {
                return View();
            }

            var vm = new DisplayEventViewModel();

            try
            {
                var Event = await _eventRepo.FindById(id);

                vm.eventImage = Event.eventImage;
                vm.ID = Event.ID;
                vm.Name = Event.Name;
                vm.Location = Event.Location;
                vm.eventDate = Event.eventDate;
                vm.Description = Event.Description;

                foreach (var a in Event.Artists)
                {
                    var m = new DisplayArtistViewModel
                    {
                        avatar = a.avatar,
                        Name = a.Name,
                        Id = a.Id
                    };


                    vm.eventArtists.Add(m);
                }

                foreach (var g in Event.Gallery)
                {
                    var item = new galleryItemViewModel {Image = g.Image};


                    vm.Gallery.Add(item);
                }


                return View(vm);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return View(vm);
            }
        }

        public async Task<IActionResult> sendMail(mail model)
        {
            try
            {
                await _msgService.SendEmailSupportAsync(model.Email, model.Subject, model.Content);

                return Ok(new { result = "ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error getting event details", ex.ToString());
                return BadRequest();
            }

        }

        public IActionResult Error()
        {
            return View();
        }

    }
}
