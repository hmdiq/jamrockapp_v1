﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Admin
{
    [Authorize(Roles = "Administrator,Blogger,Staff")]
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;
        private readonly IRepository<Post> _postRepo;
        private readonly IRepository<Rave> _eventRepo;
        private readonly IRepository<Product> _productRepo;
        private readonly IRepository<Order> _orderRepo;
        private readonly IRepository<Customer> _customerRepo;


        public AdminController(ILogger<AdminController> logger, IRepository<Post> postRepo, IRepository<Rave> eventRepo,
            IRepository<Product> productRepo, IRepository<Order> orderRepo, IRepository<Customer> customerRepo)
        {
            _logger = logger;
            _postRepo = postRepo;
            _eventRepo = eventRepo;
            _productRepo = productRepo;
            _orderRepo = orderRepo;
            _customerRepo = customerRepo;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var revenue = 0.0;
            var productsSold = 0;


            try
            {
                var orderList = await _orderRepo.GetAll();

                foreach (var item in orderList)
                {
                    //get total revenue 
                    revenue = item.OrderItems.Aggregate(revenue, (current, row) => row.Amount + current);

                    //get quantity of total products sold
                    productsSold =
                        item.OrderItems.Aggregate(productsSold, (current, row) => row.ProductQuantity + current);   
                    
                                   
                }

                var ordersCompleted = orderList.Count(u => u.Status.Equals("Complete"));

                var ordersPending = orderList.Count(u => u.Status.Equals("Pending"));

                var blogList = await _postRepo.GetAll();
                var posts = blogList.Count();

                var customerList = await _customerRepo.GetAll();
                var customers = customerList.GroupBy(c => c.Email).Count();
                
                ViewBag.revenue = revenue;
                ViewBag.productsSold = productsSold;
                ViewBag.posts = posts;
                ViewBag.customers = customers;
                ViewBag.ordersTotal = orderList.Count();
                ViewBag.ordersCompleted = ordersCompleted;
                ViewBag.ordersPending = ordersPending;


                ViewBag.lastUpdated = DateTime.Now.ToLongTimeString();
            }
            catch (Exception e)
            {
                _logger.LogError("Error in index page of admin:" + e);
            }      
            return View();
        }

        public IActionResult Documentation()
        {
            return View();
        }

    }
}
