﻿using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Models.AdminViewModels.EventViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Admin
{
    [Authorize(Roles = "Administrator,Blogger,Staff")]
    public class EventController : Controller
    {
        private readonly IRepository<Rave> _eventRepo;
        private readonly IRepositoryManager _repoManager;
        private readonly ILogger<EventController> _logger;

        public EventController(
            IRepositoryManager repoManager,
            IRepository<Rave> eventRepo,
            ILogger<EventController> logger)
        {
            _repoManager = repoManager;
            _eventRepo = eventRepo;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var eventViewModelList = new List<DisplayEventViewModel>();
            try
            {
             
                var eventList = await _eventRepo.GetAll();

                foreach (var rave in eventList)
                {
                    var eventViewModel = new DisplayEventViewModel
                    {
                        ID = rave.ID,
                        Name = rave.Name,
                        Description = rave.Description,
                        eventImage = rave.eventImage,
                        Location = rave.Location,
                        eventDate = rave.eventDate
                    };

                    eventViewModelList.Add(eventViewModel);
                }         
            }
            catch (Exception ex)
            {
              _logger.LogError("Error:"+ex);
            }
            return View(eventViewModelList);
        }

        [HttpGet]
        public IActionResult AddEvent()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddEvent(AddEventViewModel eventViewModel)
        {

            try
            {
                var imagebytes = _repoManager.UploadEventImageToCdn(eventViewModel.eventImage);

                if (string.IsNullOrEmpty(imagebytes)) return View(eventViewModel);
                var rave = new Rave
                {
                    Name = eventViewModel.Name,
                    eventImage = imagebytes,
                    Description = eventViewModel.Description,
                    Location = eventViewModel.Location,
                    CreatedOn = new DateTime(),
                    LastModified = new DateTime(),
                    eventDate = eventViewModel.eventDate
                };


                await _eventRepo.Add(rave);

                return RedirectToAction("EventDetails",new {id = rave.ID });
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex);
            
            }
            return View(eventViewModel);
        }

        public async Task<IActionResult> DeleteEvent(int? id)
        {
            var rave = await _eventRepo.FindById(id);

            await _eventRepo.Delete(rave);

            return RedirectToAction("Index");
        }
        
        public async Task<IActionResult> EventDetails(int id)
        {
            var Event = await _eventRepo.FindById(id);


            if(Event == null)
            {
                return NotFound();
            }
            var vm = new DisplayEventViewModel
            {
                eventImage = Event.eventImage,
                ID = Event.ID,
                Name = Event.Name,
                Location = Event.Location,
                eventDate = Event.eventDate,
                Description = Event.Description
            };




            foreach(var a in Event.Artists)
            {
                var m = new DisplayArtistViewModel
                {
                    avatar = a.avatar,
                    Name = a.Name,
                    Id = a.Id
                };


                vm.eventArtists.Add(m);
            }

            foreach(var g in Event.Gallery)
            {
                var item = new galleryItemViewModel
                {
                    Id = g.Id,
                    Image = g.Image
                };


                vm.Gallery.Add(item);
            }


            return View(vm);
        }

        [HttpGet]
        public IActionResult NewArtist(DisplayEventViewModel model)
        {
            ViewBag.rave = model.ID;

            return View("_artistView");
        }

        [HttpPost]
        public async Task<IActionResult> AddArtist(AddArtistViewModel model)
        {
            var selectedEvent = await _eventRepo.FindById(model.raveId);

            if (selectedEvent == null)
            {
                return NotFound();
            }
            else
            {
                var newArtist = new Artists
                {
                    avatar = _repoManager.UploadEventImageToCdn(model.avatar),
                    Name = model.Name
                };


                selectedEvent.Artists.Add(newArtist);

                await _eventRepo.Update(selectedEvent);
                return RedirectToAction("EventDetails", new { @id = model.raveId });
            }

        }

        public async Task<IActionResult> RemoveArtist(int? id,int rave)
        {
            var Event = await _eventRepo.FindById(rave);

            var artist = Event.Artists.Find(a => a.Id.Equals(id));


            Event.Artists.Remove(artist);

            await _eventRepo.Update(Event);

            return RedirectToAction("EventDetails", new { @id = rave });
        }

        public async Task<IActionResult> RemoveImage(int? id, int rave)
        {
            var Event = await _eventRepo.FindById(rave);

            var galleryImage = Event.Gallery.Find(a => a.Id.Equals(id));


            if (galleryImage == null) return NotFound();
            Event.Gallery.Remove(galleryImage);

            await _eventRepo.Update(Event);

            return RedirectToAction("EventDetails", new { id = rave });
        }

        [HttpPost]
        public async Task<IActionResult> AddImages(IFormCollection files)
        {
            if(files == null)
            {
                return BadRequest(new { error = "Files are empty" });
            }
            try
            {
                var selected = await _eventRepo.FindById(Convert.ToInt32(files["event"]));

                foreach(var image in files.Files)
                {
                    var item = new galleryItem {Image = _repoManager.UploadGalleryImageToCdn(image)};


                    selected.Gallery.Add(item);
                }

                await _eventRepo.Update(selected);

                return Ok(new { result = "Gallery Created"});
            }
            catch (Exception ex)
            {
                _logger.LogError("Error creating gallery", ex.ToString());
                return BadRequest(new { error = "Error creating gallery"});
            }
        }

        public async Task<IActionResult> UpdateEventName(int? id,string name)
        {
            try
            {
                if (id != null)
                {
                    var Event = await _eventRepo.FindById(id);

                    if (Event != null)
                    {
                        Event.Name = name;

                        await _eventRepo.Update(Event);

                        return Ok(new { result = "Ok" });
                    }

                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        public async Task<IActionResult> UpdateEventLocation(int? id, string location)
        {
            try
            {
                if (id != null)
                {
                    var Event = await _eventRepo.FindById(id);

                    if (Event != null)
                    {
                        Event.Location = location;
                        await _eventRepo.Update(Event);
                        return Ok(new { result = "Ok" });
                    }

                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        public async Task<IActionResult> UpdateEventDescription(int? id, string description)
        {
            try
            {
                if (id != null)
                {
                    var Event = await _eventRepo.FindById(id);

                    if (Event != null)
                    {
                        Event.Description = description;
                        await _eventRepo.Update(Event);
                        return Ok(new { result = "Ok" });
                    }

                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        public async Task<IActionResult> UpdateEventDate(int? id, DateTime date)
        {
            try
            {
                if (id != null)
                {
                    var Event = await _eventRepo.FindById(id);

                    if (Event != null)
                    {
                        Event.eventDate = date;
                        await _eventRepo.Update(Event);
                        return Ok(new { result = "Ok" });
                    }

                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdateEventImage(IFormCollection files)
        {
            try
            {
                var selectedEvent = await _eventRepo.FindById(Convert.ToInt32(files["id"]));

                if (selectedEvent == null)
                {
                    return BadRequest(new { error = "Error" });
                }
                selectedEvent.eventImage = _repoManager.UploadEventImageToCdn(files.Files[0]);

                await _eventRepo.Update(selectedEvent);

                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error" + ex);
                return BadRequest(new { error = "Error" });
            }

        }

    }
}
