﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Models.AdminViewModels.ProductViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Admin
{
    [Authorize(Roles = "Administrator,Blogger,Staff")]
    public class ProductController : Controller
    {
        private readonly IRepository<Product> _productRepo;
        private readonly IRepositoryManager _repoManager;
        private readonly ILogger<ProductController> _logger;
        private readonly IRepository<Order> _orderRepo;

        public ProductController(IRepository<Product> productRepo,
            IRepositoryManager repoManager,
            IRepository<Rave> eventRepo,
            ILogger<ProductController> logger,
            IRepository<Customer> customerRepo,
            IRepository<Order> orderRepo)
        {
            _productRepo = productRepo;
            _repoManager = repoManager;
            _logger = logger;
            _orderRepo = orderRepo;
        }



        [HttpGet]
        public async Task<IActionResult> Products()
        {
            var productViewModelList = new List<DisplayProductViewModel>();
            try
            {
                var productList = await _productRepo.GetAll();

                foreach (var product in productList)
                {
                    var productViewModel = new DisplayProductViewModel
                    {
                        ID = product.ID,
                        Name = product.Name,
                        Price = product.Price,
                        Quantity = product.Quantity,
                        Description = product.Description,
                        Category = product.Category,
                        productImage = product.productImageByteArray,
                        SoldQuantity = product.QuantitySold,
                        remainingQuantity = product.Quantity - product.QuantitySold
                    };

                    productViewModelList.Add(productViewModel);
                }

            }
            catch (Exception e)
            {
                _logger.LogError("Error Getting List of Products:" +e);
            }
          
            return View(productViewModelList);
        }

        [HttpGet]
        public IActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(AddProductViewModel productViewModel)
        {

            try
            {
                var imagebytes = _repoManager.UploadProductImageToCdn(productViewModel.productImage);

                if (string.IsNullOrEmpty(imagebytes)) return View(productViewModel);
                var product = new Product
                {
                    Name = productViewModel.Name,
                    Price = productViewModel.Price,
                    productImageByteArray = imagebytes,
                    Category = productViewModel.Category,
                    Description = productViewModel.Description,
                    Gender = productViewModel.Gender
                };


                await _productRepo.Add(product);

                return RedirectToAction("UpdateProduct", new { id = product.ID});
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex);
            }
            return View(productViewModel);
        }

        public async Task<IActionResult> AddColorSize(int? id, ColorSize model)
        {
            if (id == null) return BadRequest();
            var product = await _productRepo.FindById(id);

            var cs = product.ColorSizes.Find(c => c.Color.Equals(model.Color) && c.Size.Equals(model.Size));

            if (cs != null)
            {
                cs.Quantity += model.Quantity;
                cs.QuantityLeft += model.Quantity;
            }
            else
            {
                model.QuantityLeft = model.Quantity;
                product.ColorSizes.Add(model);
            }

            product.Quantity = product.Quantity + model.Quantity;

            await _productRepo.Update(product);

            return Ok(new {result = "ok"});
        }

        public async Task<IActionResult> DeleteProduct(DisplayProductViewModel displayProductViewModel)
        {
            if (displayProductViewModel == null)
            {
                return NotFound();
            }

            try
            {
                var product = await _productRepo.FindById(displayProductViewModel.ID);

                if (product != null)
                {
                    await _productRepo.Delete(product);
                }
            }
            catch (Exception ex)
            {

                _logger.LogError("exception" + ex);
            }
            return RedirectToAction("Products");

        }

        public async Task<IActionResult> DeleteMultiple(int[] selected)
        {
            if (selected == null)
            {
                return NotFound();
            }

            try
            {
                foreach (var s in selected)
                {
                    var product = await _productRepo.FindById(s);

                    if (product != null)
                    {
                        await _productRepo.Delete(product);
                    }
                }
                return RedirectToAction("Products");
            }
            catch (Exception ex)
            {

                _logger.LogError("exception" + ex);
            }

            return RedirectToAction("Products");

        }

        public async Task<IActionResult> Orders()
        {
            var orders = new List<Order>();
            try
            {
                var orderList = await _orderRepo.GetAll();

                foreach (var item in orderList)
                {
                    orders.Add(item);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error Getting Orders:"+e);
            }
           

            return View(orders);
        }

        public async Task<IActionResult> OrderDetails(int? id)
        {
            try
            {
                if (id == null) return RedirectToAction("Orders");
                var order = await _orderRepo.FindById(id);

                return View(order);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error"+ex);
                return RedirectToAction("Orders");
            }
     
        }

        [HttpGet]
        public async Task<IActionResult> UpdateProduct(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var product = await _productRepo.FindById(id);

                var productViewModel = new DisplayProductViewModel
                {
                    ID = product.ID,
                    Name = product.Name,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    Category = product.Category,
                    Description = product.Description,
                    Gender = product.Gender,
                    productImage = product.productImageByteArray,
                    ColorSizes = product.ColorSizes
                };

                return View(productViewModel);
            }
            catch (Exception)
            {
                return RedirectToAction("Products");
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductName(int id, string name)
        {
            var product = await _productRepo.FindById(id);


            if(product == null)
            {
                return BadRequest(new { error="Error"});
            }
            product.Name = name;

            await _productRepo.Update(product);

            return Ok(new {result = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductQuantity(int id, int quantity)
        {
            var product = await _productRepo.FindById(id);


            if (product == null)
            {
                return BadRequest(new { error = "Error" });
            }
            product.Quantity = product.Quantity + quantity;

            await _productRepo.Update(product);

            return Ok(new { result = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductDescription(int id, string description)
        {
            var product = await _productRepo.FindById(id);


            if (product == null)
            {
                return BadRequest(new { error = "Error" });
            }
            product.Description = description;

            await _productRepo.Update(product);

            return Ok(new { result = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductGender(int id, string gender)
        {
            var product = await _productRepo.FindById(id);


            if (product == null)
            {
                return BadRequest(new { error = "Error" });
            }
            product.Gender = gender;

            await _productRepo.Update(product);

            return Ok(new { result = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductCategory(int id, string category)
        {
            var product = await _productRepo.FindById(id);


            if (product == null)
            {
                return BadRequest(new { error = "Error" });
            }
            product.Category = category;

            await _productRepo.Update(product);

            return Ok(new { result = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProductImage(IFormCollection files)
        {
            try
            {
                var product = await _productRepo.FindById(Convert.ToInt32(files["id"]));

                if (product == null)
                {
                    return BadRequest(new { error = "Error" });
                }
                product.productImageByteArray = _repoManager.UploadProductImageToCdn(files.Files[0]);

                await _productRepo.Update(product);

                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error" + ex);
                return BadRequest(new {error = "Error"});
            }
          
        }

        public async Task<IActionResult> UpdateProductStatus(int? id)
        {
            try
            {
                if (id == null) return RedirectToAction("Orders");
                var order = await _orderRepo.FindById(id);

                order.Status = "Complete";

                await _orderRepo.Update(order);
                return RedirectToAction("Orders");
            }
            catch (Exception e)
            {
                _logger.LogError("Error updating order status:" +e);
                return RedirectToAction("Orders");
            }   
        }

    }
}
