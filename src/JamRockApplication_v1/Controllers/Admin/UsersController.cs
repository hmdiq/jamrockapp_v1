﻿using System;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AccountViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Admin
{
    [Authorize(Roles = "Administrator,Blogger,Staff")]
    public class UsersController : Controller
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IUserManager<RegisterViewModel> _userService;

        public UsersController(IHostingEnvironment environment,
            ILogger<UsersController> logger,
            IRepository<Customer> customerRepo,
            IUserManager<RegisterViewModel> userService)
        {
            _logger = logger;
            _customerRepo = customerRepo;
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Customers()
        {
            var displayList = new List<Customer>();

            try
            {
                var result = await _customerRepo.GetAll();

                displayList = result.GroupBy(u => u.Email).Select(g => g.First()).ToList();

                return View(displayList);
            }
            catch (Exception ex)
            {

                _logger.LogError("Error:"+ex);
                return View(displayList);
            }           
        }


        [HttpGet]
        public IActionResult NewUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> NewUser(RegisterViewModel model, string returnUrl = null)
        {
            try
            {
                if (!ModelState.IsValid) return View();
                var status = await _userService.Registration(model);
                ViewBag.status = status ? "Successful, New User Created" : "Error, User Not Created";
                return View();
            }
            catch (Exception e)
            {
                _logger.LogError("Error creating new user"+e);
                ViewBag.status = "Exception Error";
                return View();
            }
        }
    }
}
