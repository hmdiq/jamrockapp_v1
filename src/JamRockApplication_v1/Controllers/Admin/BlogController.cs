﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JamRockApplication_v1.Interfaces;
using Microsoft.Extensions.Logging;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Models.AdminViewModels.BlogViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace JamRockApplication_v1.Controllers.Admin
{
    
    public class BlogController : Controller
    {
        private readonly IRepository<Post> _postRepo;
        private readonly ILogger<BlogController> _logger;
        private readonly IRepositoryManager _repoManager;

        public BlogController(IRepository<Post> postRepo, ILogger<BlogController> logger, IRepositoryManager repoManager)
        {
            _logger = logger;
            _postRepo = postRepo;
            _repoManager = repoManager;
        }

        public async Task<IActionResult> Index(string category)
        {
            var pvm = new postPageViewModel();

            try
            {
                var postList = await _postRepo.GetAll();
                var popularPosts = await _postRepo.FindTop();

                if (postList == null)
                {
                    return View(pvm);
                }

                if (string.IsNullOrEmpty(category))
                {
                    foreach (var p in postList)
                    {
                        var newPost = new DisplayPostViewModel
                        {
                            title = p.title,
                            content = p.content,
                            CreatedOn = p.CreatedOn,
                            Id = p.Id,
                            newsImage = p.newsImage,
                            Category = p.Category
                        };
                        pvm.allPosts.Add(newPost);
                    }
                }

                foreach (var p in postList.Where(s => s.Category.Equals(category)))
                {
                    var newPost = new DisplayPostViewModel
                    {
                        title = p.title,
                        content = p.content,
                        CreatedOn = p.CreatedOn,
                        Id = p.Id,
                        newsImage = p.newsImage,
                        Category = p.Category
                    };
                    pvm.allPosts.Add(newPost);
                }

                foreach (var p in popularPosts)
                {
                    var newPost = new DisplayPostViewModel
                    {
                        title = p.title,
                        content = p.content,
                        CreatedOn = p.CreatedOn,
                        Id = p.Id,
                        newsImage = p.newsImage,
                        Category = p.Category
                    };


                    pvm.popularPosts.Add(newPost);

                }

                return View(pvm);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return View(pvm);

            }
        }


        [Authorize(Roles = "Administrator,Blogger,Staff")]
        public async Task<IActionResult> Posts()
        {
            var modelList = new List<Post>();

            try
            {
                var postList = await _postRepo.GetAll();

                if (postList == null)
                {
                    return View(modelList);
                }
                foreach (var p in postList)
                {
                    var newPost = new Post
                    {
                        title = p.title,
                        content = p.content,
                        CreatedOn = p.CreatedOn,
                        Id = p.Id
                    };


                    modelList.Add(newPost);

                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error retrieving List of Posts:" +e);
            }

            return View(modelList);
        }


        [Authorize(Roles = "Administrator,Blogger,Staff")]
        [HttpGet]
        public IActionResult AddPost()
        {
            return View();
        }


        [Authorize(Roles = "Administrator,Blogger,Staff")]
        [HttpPost]
        public async Task<IActionResult> AddPost(AddPostViewModel post)
        {
            if (!ModelState.IsValid) return View(post);
            try
            {
                var p = new Post
                {
                    title = post.title,
                    CreatedOn = DateTime.Now,
                    content = post.content,
                    newsImage = _repoManager.UploadPostImageToCdn(post.newsImage),
                    LastModified = DateTime.Now,
                    Category = post.Category
                };

                await _postRepo.Add(p);
                return RedirectToAction("Posts");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error Inserting Item";
                    _logger.LogError("Error Adding Blog Post: "+e);
                return View(post);

            }
  
        }

        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                var result = await _postRepo.FindById(id);
                if (result == null) return View();
                var pvm = new DisplayPostViewModel
                {
                    Id = result.Id,
                    title = result.title,
                    newsImage = result.newsImage,
                    content = result.content,
                    CreatedOn = result.CreatedOn,
                    Category = result.Category
                };

                return View(pvm);
            }
            catch (Exception e)
            {
                _logger.LogError("Error retrieving Post:" +e);
                return RedirectToAction("Posts");
            }
         
        }


        [Authorize(Roles = "Administrator,Blogger,Staff")]
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                var postFound = await _postRepo.FindById(id);

                if (postFound != null)
                {
                    await _postRepo.Delete(postFound);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception e)
            {
               _logger.LogError("Error Deleting Post:" +e);
            }

            return RedirectToAction("Posts");
        }


        [Authorize(Roles = "Administrator,Blogger,Staff")]
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                var post = await _postRepo.FindById(id);

                if (post == null) return NotFound();


                var pvm = new DisplayPostViewModel
                {
                    Id = post.Id,
                    title = post.title,
                    content = post.content,
                    CreatedOn = post.CreatedOn,
                    Category = post.Category,
                    newsImage = post.newsImage

                };

                return View(pvm);
            }
            catch (Exception e)
            {
               _logger.LogError("Error is: "+e);
                return RedirectToAction("Posts");
            }
        
        }

        public async Task<IActionResult> UpdatePost(AddPostViewModel post)
        {
            if(post == null)
            {
                return RedirectToAction("Posts");
            }

            try
            {
                var p = new Post
                {
                    Id = post.Id,
                    title = post.title,
                    CreatedOn = post.CreatedOn,
                    content = post.content,
                    newsImage = _repoManager.UploadPostImageToCdn(post.newsImage),
                    LastModified = post.CreatedOn,
                    Category = post.Category
                };

                await _postRepo.Update(p);
            }
            catch (Exception e)
            {
                _logger.LogError("Error Updating Post:" +e);
            }

            return RedirectToAction("Posts");
        }

        public async Task<IActionResult> UpdateBlogTitle(int? id, string title)
        {
            try
            {
                if (id == null) return BadRequest(new {error = "Field 'Blog Title' is Empty"});
                var Event = await _postRepo.FindById(id);

                if (Event == null) return BadRequest();
                Event.title = title;

                await _postRepo.Update(Event);
                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        public async Task<IActionResult> UpdateBlogContent(int? id, string content)
        {
            try
            {
                if (id == null) return BadRequest( new {error = "Field 'Blog Content' is empty"});
                var Event = await _postRepo.FindById(id);

                if (Event == null) return BadRequest();
                Event.content = content;

                await _postRepo.Update(Event);
                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        public async Task<IActionResult> UpdateBlogCategory(int? id, string category)
        {
            try
            {
                if (id == null) return BadRequest();
                var Event = await _postRepo.FindById(id);

                if (Event == null) return BadRequest();
                Event.Category = category;

                await _postRepo.Update(Event);
                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error", ex.ToString());
                return BadRequest();
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdateBlogImage(IFormCollection files)
        {
            try
            {
                var selectedEvent = await _postRepo.FindById(Convert.ToInt32(files["id"]));

                if (selectedEvent == null)
                {
                    return BadRequest(new { error = "Error" });
                }
                selectedEvent.newsImage = _repoManager.UploadPostImageToCdn(files.Files[0]);

                await _postRepo.Update(selectedEvent);

                return Ok(new { result = "Ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error" + ex);
                return BadRequest(new { error = "Error" });
            }

        }
    }
}
