﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http.Authentication;

namespace JamRockApplication_v1.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task SignIn()
        {
            try
            {
                await HttpContext.Authentication.ChallengeAsync(
                    OpenIdConnectDefaults.AuthenticationScheme, new AuthenticationProperties { RedirectUri = "/Shop/Index" });
            }
            catch (Exception e)
            {
               _logger.LogError("Error Logging in"+e);
            }
        }

        [HttpGet]
        public IActionResult SignOut()
        {
            try
            {
                return SignOut(
                    new AuthenticationProperties { RedirectUri = Url.Action("SignedOut"), AllowRefresh = false, IsPersistent = false },
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    OpenIdConnectDefaults.AuthenticationScheme);
            }
            catch (Exception e)
            {
                _logger.LogError("Error Logging in" + e);
                return RedirectToAction("Index", "Shop");
            }


        }

        [HttpGet]
        public IActionResult SignedOut()
        {
            try
            {
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                _logger.LogError("Error Logging in" + e);
                return RedirectToAction("Index", "Home");
            }

        }
    }
}
