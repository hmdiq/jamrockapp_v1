﻿namespace JamRockApplication_v1.Controllers.Shop
{
    public class ptransactionVmcs
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string refNumber { get; set; }
        public string DeliveryMode { get; set; }
    }
}