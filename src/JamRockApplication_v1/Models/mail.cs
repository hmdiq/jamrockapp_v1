﻿using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models
{
    public class mail
    {
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Subject { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Content { get; set; }
    }
}
