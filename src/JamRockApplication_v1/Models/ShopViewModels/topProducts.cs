﻿using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.ShopViewModels
{
    public class topProducts
    {
        public int ID { get; set; }
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public double Price { get; set; }

        [Display(Name = "Product Image")]
        public string productImage { get; set; }
    }
}
