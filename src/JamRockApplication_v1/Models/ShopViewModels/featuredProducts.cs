﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JamRockApplication_v1.Models.AdminModels;

namespace JamRockApplication_v1.Models.ShopViewModels
{
    public class featuredProducts
    {
        public featuredProducts()
        {
            ColorSizes = new List<ColorSize>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public double Price { get; set; }

        [Display(Name = "Qauntity Added")]
        public int Quantity { get; set; }

        [Display(Name = "Qauntity Remaining")]
        public int remainingQuantity { get; set; }

        [Display(Name = "Qauntity Sold")]
        public int SoldQuantity { get; set; }
        public string Description { get; set; }

        [Display(Name = "Product Image")]
        public string productImage { get; set; }

        public List<ColorSize> ColorSizes { get; set; }
    }
}
