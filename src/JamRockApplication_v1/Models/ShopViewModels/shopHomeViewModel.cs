﻿using System.Collections.Generic;

namespace JamRockApplication_v1.Models.ShopViewModels
{
    public class shopHomeViewModel
    {

        public shopHomeViewModel()
        {
            Featured = new List<featuredProducts>();
            New = new List<newProducts>();
            Top = new List<topProducts>();
            Random = new List<randomProducts>();
        }
        public List<featuredProducts> Featured { get; set; }

        public List<newProducts> New { get; set; }

        public List<topProducts> Top { get; set; }

        public List<randomProducts> Random { get; set; }
    }
}
