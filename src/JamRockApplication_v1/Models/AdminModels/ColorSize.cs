﻿namespace JamRockApplication_v1.Models.AdminModels
{
    public class ColorSize
    {
        public int Id { get; set; }

        public string Color { get; set; }

        public string Size { get; set; }

        public int Quantity { get; set; }

        public int QuantityLeft { get; set; }

    }
}
