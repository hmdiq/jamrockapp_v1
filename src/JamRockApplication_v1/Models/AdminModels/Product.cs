﻿using System;
using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminModels
{
    public class Product
    {
        public Product()
        {
                ColorSizes = new List<ColorSize>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Gender { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public int QuantitySold { get; set; }
        public string Description { get; set; }
        public string productImageByteArray { get; set; }
        public DateTime createdOn { get; set; } = DateTime.Now;

        public List<ColorSize> ColorSizes { get; set; }
    }
}
