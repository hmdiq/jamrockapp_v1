﻿using System;

namespace JamRockApplication_v1.Models.AdminModels
{
    public class Post
    {
        public int Id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string newsImage { get; set; }

        public string Category { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public DateTime LastModified { get; set; } = DateTime.Now;
    }
}
