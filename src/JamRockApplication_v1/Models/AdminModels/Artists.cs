﻿namespace JamRockApplication_v1.Models.AdminModels
{
    public class Artists
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string avatar { get; set; }

        public int RaveId { get; set; }

        public Rave Rave { get; set; }


    }
}
