﻿using System;
using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminModels
{
    public class Rave
    {

        public Rave()
        {
            Artists = new List<Artists>();
            Gallery = new List<galleryItem>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime eventDate { get; set; }
        public string eventImage { get; set; }

        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModified { get; set; }

        public List<Artists> Artists { get; set; }

        public List<galleryItem> Gallery { get; set; }
    }
}
