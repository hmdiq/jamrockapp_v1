﻿using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminModels
{
    public class Order
    {
        public Order()
        {
                OrderItems = new List<cartItem>();
            Status = "Pending";
        }
        public int Id { get; set; }

        public List<cartItem> OrderItems { get; set; }

        public string DeliveryType { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string Status { get; set; }
    }
}
