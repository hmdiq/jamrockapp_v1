﻿using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
