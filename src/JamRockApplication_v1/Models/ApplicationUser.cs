﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace JamRockApplication_v1.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
