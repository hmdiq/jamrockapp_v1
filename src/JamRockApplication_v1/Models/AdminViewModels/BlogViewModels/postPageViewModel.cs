﻿using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminViewModels.BlogViewModels
{
    public class postPageViewModel
    {
        public postPageViewModel()
        {
            allPosts = new List<DisplayPostViewModel>();
            popularPosts = new List<DisplayPostViewModel>();
        }
        public List<DisplayPostViewModel> allPosts { get; set; }

        public List<DisplayPostViewModel> popularPosts { get; set; }
    }
}
