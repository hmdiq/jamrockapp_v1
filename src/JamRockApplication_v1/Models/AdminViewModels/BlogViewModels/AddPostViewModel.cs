﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.BlogViewModels
{
    public class AddPostViewModel
    {
        public int Id { get; set; }


        [StringLength(60, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string title { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string content { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Category { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public IFormFile newsImage { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public DateTime LastModified { get; set; } = DateTime.Now;
    }
}
