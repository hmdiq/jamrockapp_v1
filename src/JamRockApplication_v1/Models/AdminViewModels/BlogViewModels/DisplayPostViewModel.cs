﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.BlogViewModels
{
    public class DisplayPostViewModel
    {
        public int Id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string newsImage { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Category { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime LastModified { get; set; }
    }
}
