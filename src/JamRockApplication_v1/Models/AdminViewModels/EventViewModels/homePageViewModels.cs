﻿using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class homePageViewModels
    {

        public homePageViewModels()
        {
            previousEvents = new List<DisplayEventViewModel>();
            upComingEvents = new List<DisplayEventViewModel>();
        }
        public DisplayEventViewModel latestModel { get; set; }

        public List<DisplayEventViewModel> previousEvents { get; set; }

        public List<DisplayEventViewModel> upComingEvents { get; set; }
    }
}
