﻿using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class FestivalViewModel
    {
        public FestivalViewModel()
        {
            events = new List<DisplayEventViewModel>();
        }
        public List<DisplayEventViewModel> events { get; set; }
    }
}
