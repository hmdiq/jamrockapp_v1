﻿namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class DisplayArtistViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string avatar { get; set; }

    }
}
