﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class AddArtistViewModel
    {
        public int Id { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }


        [Required]
        [DataType(DataType.Upload)]
        public IFormFile avatar { get; set; }


        public int raveId { get; set; }
    }
}
