﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class AddEventViewModel
    {
        public int ID { get; set; }

        [StringLength(150, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [StringLength(150, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string Location { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime eventDate { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public IFormFile eventImage { get; set; }


        [StringLength(1500, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }

        [DataType(DataType.Date)]
        public DateTime LastModified { get; set; }
    }
}
