﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.EventViewModels
{
    public class DisplayEventViewModel
    {
        public DisplayEventViewModel()
        {
            eventArtists = new List<DisplayArtistViewModel>();
            Gallery = new List<galleryItemViewModel>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime eventDate { get; set; }
        public string eventImage { get; set; }

        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModified { get; set; }

        public List<DisplayArtistViewModel> eventArtists { get; set; }

        public List<galleryItemViewModel> Gallery { get; set; }
    }
}
