﻿
using JamRockApplication_v1.Models.AdminModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminViewModels.CartViewModels
{
    public class CartViewModel
    {
        public CartViewModel()
        {
            cartItems = new List<cartItem>();
        }

        public int Quantity { get; set; }

        public List<cartItem> cartItems { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public double totalCost { get; set; }

    }
}
