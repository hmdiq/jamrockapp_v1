﻿using JamRockApplication_v1.Models.AdminViewModels.ProductViewModels;
using System.Collections.Generic;

namespace JamRockApplication_v1.Models.AdminViewModels.CartViewModels
{
    public class CartItemViewModel
    {
        public int Id { get; set; }

        public List<DisplayProductViewModel> product { get; set; }
    }
}
