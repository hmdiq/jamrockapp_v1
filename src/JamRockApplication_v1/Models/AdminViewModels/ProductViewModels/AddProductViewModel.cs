﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace JamRockApplication_v1.Models.AdminViewModels.ProductViewModels
{
    public class AddProductViewModel
    {
        public int ID { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Category { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Gender { get; set; }

        [DataType(DataType.Currency)]
        [Required]
        public double Price { get; set; }

        [Required]
        public int Quantity { get; set; }

        [StringLength(200, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public IFormFile productImage { get; set; }
    }
}
