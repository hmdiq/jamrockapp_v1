﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JamRockApplication_v1.Models.AdminModels
{
    public class Cart
    {
        public Cart()
        {
            cartItems = new List<cartItem>();
            createdOn = new DateTime().Date;
            lastModified = new DateTime().Date;
            Quantity = 0;
            totalCost = 0;
        }

        [Key]
        public string Id { get; set; }

        public int Quantity { get; set; }

        public virtual List<cartItem> cartItems { get; set; }

        public DateTime createdOn { get; set; }

        public DateTime lastModified { get; set; }

        public double totalCost { get; set; }
    }
}
