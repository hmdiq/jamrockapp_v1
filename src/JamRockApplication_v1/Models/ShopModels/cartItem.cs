﻿namespace JamRockApplication_v1.Models.AdminModels
{
    public class cartItem
    {
        public int Id { get; set; }

        public int ProductSku { get; set; }

        public string ProductName { get; set; }

        public string ProductImage { get; set; }

        public int ProductQuantity { get; set; }

        public double ProductPrice { get; set; }

        public string ProductColor { get; set; }

        public string ProductSize{ get; set; }

        public double Amount { get; set; }

        public string CartId { get; set; }

        public Cart Cart { get; set; }
    }
}
