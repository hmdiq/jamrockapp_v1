﻿using JamRockApplication_v1.Data;
using JamRockApplication_v1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace JamRockApplication_v1.Seeders
{
    public class UserSeeder
    {    
        public static async void Initialize(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> _roleManager)
        { 
            string[] roles = { "SuperAdmin", "Admin", "Customer" };

            //first ensure the database has been created
            context.Database.Migrate();



            //check if users table has any users 
            if (context.Users.Any())
            {
                return;
            }
            else
            {


                var user = new ApplicationUser { UserName = "heuristix@live.com", Email = "heuristix@live.com" };
                var result = await userManager.CreateAsync(user, "Heuristix1#");

                if (result.Succeeded)
                {
                    var roleExist = await _roleManager.RoleExistsAsync("SuperAdmin");

                    if (!roleExist)
                    {
                        foreach (var role in roles)
                        {
                            try
                            {
                                await _roleManager.CreateAsync(new IdentityRole(role));
                            }
                            catch (System.Exception)
                            {
                                throw;
                            }
                        }
                        await userManager.AddToRoleAsync(user, "SuperAdmin");
                    }
                    else
                    {
                        await userManager.AddToRoleAsync(user, "SuperAdmin");
                    }
                }
            }


            context.SaveChanges();
        }
    }
}
