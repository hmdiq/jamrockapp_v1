﻿using JamRockApplication_v1.Data;
using Microsoft.EntityFrameworkCore;

namespace JamRockApplication_v1.Seeders
{
    public class ProductsSeeder
    {
        public static void Initialize(ShopContext context)
        {
            context.Database.Migrate();
            context.Database.EnsureCreated();
          

            context.SaveChanges();
        }
    }
}
