﻿using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Services.AdminServices
{
    public class RaveRepo : IRepository<Rave>
    {
        private readonly ILogger<RaveRepo> _logger;
        private ShopContext _entities = new ShopContext();

        public ShopContext Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public RaveRepo(ShopContext entities, ILogger<RaveRepo> logger)
        {
            _entities = entities;
            _logger = logger;
        }

        public async Task Add(Rave entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Set<Rave>().Add(entity);
            await _entities.SaveChangesAsync();
        }

        public async Task Delete(Rave entity)
        {

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            try
            {
                _entities.Set<Rave>().Remove(entity);
                await _entities.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                _logger.LogError("exception" + ex.ToString());
            }
        }

        public async Task<Rave> FindById(string Id)
        {
            try
            {
                return await _entities.Events.Include(s => s.Artists).Include(g => g.Gallery).FirstOrDefaultAsync(x => x.ID.Equals(Id));
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex.ToString());
                return null;
            }
        }

        public async Task<Rave> FindById(int? Id)
        {
            try
            {
                return await _entities.Events.Include(s => s.Artists).Include(g => g.Gallery).FirstOrDefaultAsync(x => x.ID.Equals(Id));
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        public async Task<IQueryable<Rave>> GetAll()
        {
            IQueryable<Rave> query = _entities.Set<Rave>().Include(s => s.Artists);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task Update(Rave entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Entry(entity).State = EntityState.Modified;
            await _entities.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }

        public async Task<Rave> FindLast()
        {
            try
            {
                return await _entities.Events.FirstOrDefaultAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        //returns top upcoming event
        public async Task<Rave> FindLatest()
        {
            try
            {
                return await _entities.Events.Where(p =>p .eventDate > DateTime.Now).LastOrDefaultAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        public Task<IQueryable<Rave>> FindRandom()
        {
            throw new NotImplementedException();
        }

        //returns last 3 previous events
        public async Task<IQueryable<Rave>> FindTop()
        {
            var query = _entities.Set<Rave>().Where(p => p.eventDate < DateTime.Now).Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        //returns all upcoming events
        public async Task<IQueryable<Rave>> FindNewest()
        {
            var query = _entities.Set<Rave>().Where(p => p.eventDate > DateTime.Now).Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<IQueryable<Rave>> Findfeatured()
        {
            var query = _entities.Set<Rave>().OrderBy(p => p.CreatedOn).Take(1);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public Task<IQueryable<Rave>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
