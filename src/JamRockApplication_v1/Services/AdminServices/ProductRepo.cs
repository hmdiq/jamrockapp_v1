﻿using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Services.AdminServices
{
    public class ProductRepo : IRepository<Product>
    {
        private readonly ILogger<ProductRepo> _logger;
        private ShopContext _entities = new ShopContext();

        public ShopContext Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public ProductRepo(ShopContext entities, ILogger<ProductRepo> logger)
        {
            _entities = entities;
            _logger = logger;
        }

        async Task IRepository<Product>.Add(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Set<Product>().Add(entity);
            await _entities.SaveChangesAsync();
        }

        async Task IRepository<Product>.Delete(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            try
            {
                _entities.Set<Product>().Remove(entity);
                await _entities.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                _logger.LogError("exception" + ex.ToString());
            }

        }


        async Task<Product> IRepository<Product>.FindById(int? Id)
        {
            try
            {
                return await _entities.Products.Include(cs => cs.ColorSizes).FirstOrDefaultAsync(x => x.ID.Equals(Id));
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }

        }

        async Task<Product> IRepository<Product>.FindById(string Id)
        {
            try
            {
                var result = await _entities.Products.FirstOrDefaultAsync(x => x.ID.Equals(Id));
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex.ToString());
                return null;
            }
        }

        async Task<IQueryable<Product>> IRepository<Product>.GetAll()
        {
            IQueryable<Product> query = _entities.Set<Product>().Include(cs => cs.ColorSizes);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        async Task IRepository<Product>.Update(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Entry(entity).State = EntityState.Modified;
            await _entities.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }

        public Task<Product> FindLast()
        {
            throw new NotImplementedException();
        }

        Task<Product> IRepository<Product>.FindLast()
        {
            throw new NotImplementedException();
        }

        Task<Product> IRepository<Product>.FindLatest()
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Product>> FindRandom()
        {
            var query = _entities.Set<Product>().OrderBy(p => p.Category).Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<IQueryable<Product>> FindTop()
        {
            var query = _entities.Set<Product>().OrderBy(p => p.QuantitySold).Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<IQueryable<Product>> FindNewest()
        {
            var query = _entities.Set<Product>().OrderByDescending(p => p.createdOn).Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<IQueryable<Product>> Findfeatured()
        {
            var query = _entities.Set<Product>().Take(3);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task<IQueryable<Product>> GetAllInCategory(string category)
        {
            IQueryable<Product> query;
            if (category.Equals("Men") || category.Equals("Women"))
            {
                 query = _entities.Set<Product>().Include(cs => cs.ColorSizes).Where(cs => cs.Gender.Equals(category) || cs.Category.Equals(category) || cs.Gender.Equals("Both"));
            }
            else
            {
                query = _entities.Set<Product>().Include(cs => cs.ColorSizes).Where(cs => cs.Category.Equals(category));
            }
          
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }
    }
}
