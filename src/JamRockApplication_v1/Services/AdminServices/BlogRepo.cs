﻿using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Services.AdminServices
{
    public class BlogRepo : IRepository<Post>
    {
        private readonly ILogger<BlogRepo> _logger;
        private ShopContext _entities = new ShopContext();

        public ShopContext Context
        {
            get { return _entities; }
            set { _entities = value; }
        }


        public BlogRepo(ShopContext entities, ILogger<BlogRepo> logger)
        {
            _logger = logger;
            _entities = entities;
        }
        public async Task Add(Post entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Set<Post>().Add(entity);
            await _entities.SaveChangesAsync();
        }

        public async Task Delete(Post entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            try
            {
                _entities.Set<Post>().Remove(entity);
                await _entities.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                _logger.LogError("exception" + ex.ToString());
            }
        }

        public async Task<Post> FindById(string Id)
        {
            try
            {
                return await _entities.Posts.FirstOrDefaultAsync(x => x.Id.Equals(Id));
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        public async Task<Post> FindById(int? Id)
        {
            try
            {
                return await _entities.Posts.FirstOrDefaultAsync(x => x.Id.Equals(Id));
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        public Task<Post> FindLast()
        {
            throw new NotImplementedException();
        }

        public Task<Post> FindLatest()
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Post>> GetAll()
        {
            IQueryable<Post> query = _entities.Set<Post>();
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task Update(Post entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Entry(entity).State = EntityState.Modified;
            await _entities.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }

        public Task<IQueryable<Post>> FindRandom()
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Post>> FindTop()
        {
            IQueryable<Post> query = _entities.Set<Post>().OrderBy(b =>b.CreatedOn);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public Task<IQueryable<Post>> FindNewest()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Post>> Findfeatured()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Post>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
