﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AccountViewModels;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Group = Microsoft.Azure.ActiveDirectory.GraphClient.Group;

namespace JamRockApplication_v1.Services
{
    public class UserService:IUserManager<RegisterViewModel>
    {
        private const string TenantName = "jamrockng.onmicrosoft.com";
        private const string AppId = "8a6d282b-663a-45f7-8033-3d480994d851";
        private const string AppSecretKey = "aYo+Y6Ts65CsBNtqFgREXJ53CY8Gc35d44nOQxekiTE=";
        private const string AadGraphUri = "https://graph.windows.net";
        private const string Authority = "https://login.microsoftonline.com/jamrockng.onmicrosoft.com";
        public static string AccessToken;
        public ActiveDirectoryClient AadClient;
        private readonly ILogger<UserService> _logger;


        public UserService(ILogger<UserService> logger)
        {
            _logger = logger;
            var authContext = new AuthenticationContext(Authority);
            var clientCredential = new ClientCredential(AppId, AppSecretKey);

            var graphUri = new Uri(AadGraphUri);
            var serviceRoot = new Uri(graphUri, TenantName);
            AadClient = new ActiveDirectoryClient(serviceRoot, async () => await AcquireGraphApiAccessToken(AadGraphUri, authContext, clientCredential));
        }

        public async Task<bool> Registration(RegisterViewModel user)
        {
            var selectedGroup = (Group)AadClient.Groups.Where(g => g.DisplayName.Equals(user.Role)).ExecuteAsync().Result.CurrentPage.ToList().FirstOrDefault();

            try
            {
                var u = new User
                {
                    DisplayName = user.Name,
                    AccountEnabled = true,
                    UserPrincipalName = user.Name + "@" + TenantName,
                    MailNickname = user.Name,
                    CreationType = "LocalAccount",
                    SignInNames = new List<SignInName>
                    {
                        new SignInName
                        {
                            Type = "emailAddress",
                            Value = user.Email
                        }
                    },
                    PasswordProfile = new PasswordProfile
                    {
                        Password = user.Password,
                        ForceChangePasswordNextLogin = false
                    }
                };

                await AadClient.Users.AddUserAsync(u);

                var selectedUser = AadClient.Users.Where(g => g.DisplayName.Equals(user.Name)).ExecuteAsync().Result.CurrentPage.ToList().FirstOrDefault();

                if (selectedGroup == null) return true;
                selectedGroup.Members.Add(selectedUser as DirectoryObject);
                await selectedGroup.UpdateAsync();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError("Exception creating user:" + e);
                return false;
            }
        }

        private static async Task<string> AcquireGraphApiAccessToken(string graphApiUrl, AuthenticationContext authContext, ClientCredential clientCredential)
        {
            AuthenticationResult result = null;
            var retryCount = 0;
            bool retry;
            do
            {
                retry = false;
                try
                {
                    result = await authContext.AcquireTokenAsync(graphApiUrl, clientCredential);
                    AccessToken = result.AccessToken;
                }
                catch (AdalException ex)
                {
                    if (ex.ErrorCode != "temporarily_unavailable") continue;
                    retry = true;
                    retryCount++;
                    await Task.Delay(3000);
                }
            } while (retry && (retryCount < 3));

            return result?.AccessToken;
        }
    }
}
