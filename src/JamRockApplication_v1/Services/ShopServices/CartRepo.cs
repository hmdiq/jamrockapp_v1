﻿using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Services.ShopServices
{
    public class CartRepo : IRepository<Cart>
    {
        private readonly ILogger<CartRepo> _logger;

        public const string CartSessionKey = "cartId";
        public string ShoppingCartId { get; set; }

        public ShopContext Context { get; set; }

        public CartRepo(ShopContext entities, ILogger<CartRepo> logger)
        {
            Context = entities;
            _logger = logger;
        }

        public async Task Add(Cart entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Set<Cart>().Add(entity);
            await Context.SaveChangesAsync();
        }

        public async Task Delete(Cart entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Set<Cart>().Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task<Cart> FindById(string Id)
        {
            try
            {

                var result = await Context.Carts.Include(s => s.cartItems).FirstOrDefaultAsync(x => x.Id.Equals(Id));
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("exception" + ex.ToString());
                return null;
            }
        }

        public Task<Cart> FindById(int? Id)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Cart>> GetAll()
        {
            IQueryable<Cart> query = Context.Set<Cart>().Include(s => s.cartItems);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task Update(Cart entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Entry(entity).State = EntityState.Modified;
            await Context.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }

        public Task<Cart> FindById(int Id)
        {
            throw new NotImplementedException();
        }

        public Task<Cart> FindLast()
        {
            throw new NotImplementedException();
        }

        public Task<Cart> FindLatest()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Cart>> FindRandom()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Cart>> FindTop()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Cart>> FindNewest()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Cart>> Findfeatured()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Cart>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
