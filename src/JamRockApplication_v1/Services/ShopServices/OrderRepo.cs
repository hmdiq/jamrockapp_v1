﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace JamRockApplication_v1.Services.ShopServices
{
    public class OrderRepo : IRepository<Order>
    {
        private readonly ILogger<OrderRepo> _logger;
        public ShopContext Context { get; set; }


        public OrderRepo(ShopContext entities, ILogger<OrderRepo> logger)
        {
            Context = entities;
            _logger = logger;
        }
        public async Task<IQueryable<Order>> GetAll()
        {
            IQueryable<Order> query = Context.Set<Order>().Include(s => s.Customer).Include(i => i.OrderItems);
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task Add(Order entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Set<Order>().Add(entity);
            await Context.SaveChangesAsync();
        }

        public async Task Update(Order entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Entry(entity).State = EntityState.Modified;
            await Context.SaveChangesAsync();

            await Task.FromResult(TimeSpan.FromMilliseconds(1));
        }

        public async Task Delete(Order entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Set<Order>().Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task<Order> FindById(int? Id)
        {
            try
            {
                return await Context.Orders.Include(c => c.Customer).Include(o => o.OrderItems).FirstOrDefaultAsync(x => x.Id.Equals(Id));
            }
            catch (Exception exception)
            {
                _logger.LogError("Error while trying to find product by Id", exception.ToString());
                return null;
            }
        }

        public  Task<Order> FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public  Task<Order> FindLast()
        {
            throw new NotImplementedException();
        }

        public  Task<Order> FindLatest()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Order>> FindRandom()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Order>> FindTop()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<Order>> FindNewest()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Order>> Findfeatured()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Order>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
