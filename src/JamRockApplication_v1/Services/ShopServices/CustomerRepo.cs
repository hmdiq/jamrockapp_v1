﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.Extensions.Logging;

namespace JamRockApplication_v1.Services.ShopServices
{
    public class CustomerRepo : IRepository<Customer>
    {
        private readonly ILogger<CustomerRepo> _logger;
        public ShopContext Context { get; set; }


        public CustomerRepo(ShopContext entities, ILogger<CustomerRepo> logger)
        {
            Context = entities;
            _logger = logger;
        }
        public async Task<IQueryable<Customer>> GetAll()
        {
            IQueryable<Customer> query = Context.Set<Customer>();
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public Task Add(Customer entity)
        {
            throw new NotImplementedException();
        }

        public  Task Update(Customer entity)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Customer entity)
        {
            throw new NotImplementedException();
        }

        public Task<Customer> FindById(int? Id)
        {
            throw new NotImplementedException();
        }

        public  Task<Customer> FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public  Task<Customer> FindLast()
        {
            throw new NotImplementedException();
        }

        public  Task<Customer> FindLatest()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Customer>> FindRandom()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Customer>> FindTop()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Customer>> FindNewest()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Customer>> Findfeatured()
        {
            throw new NotImplementedException();
        }

        public  Task<IQueryable<Customer>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
