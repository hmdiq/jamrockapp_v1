﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JamRockApplication_v1.Data;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models;
using Microsoft.Extensions.Logging;

namespace JamRockApplication_v1.Services
{
    public class NewsLetterService : IRepository<NewsList>
    {
        private readonly ILogger<NewsLetterService> _logger;

        public const string CartSessionKey = "cartId";
        public string ShoppingCartId { get; set; }

        public ShopContext Context { get; set; }

        public NewsLetterService(ShopContext entities, ILogger<NewsLetterService> logger)
        {
            Context = entities;
            _logger = logger;
        }

        public async Task<IQueryable<NewsList>> GetAll()
        {
            IQueryable<NewsList> query = Context.Set<NewsList>();
            await Task.FromResult(TimeSpan.FromMilliseconds(1));
            return query;
        }

        public async Task Add(NewsList entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Set<NewsList>().Add(entity);
            await Context.SaveChangesAsync();

        }

        public Task Update(NewsList entity)
        {
            throw new NotImplementedException();
        }

        public Task Delete(NewsList entity)
        {
            throw new NotImplementedException();
        }

        public Task<NewsList> FindById(int? Id)
        {
            throw new NotImplementedException();
        }

        public Task<NewsList> FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public Task<NewsList> FindLast()
        {
            throw new NotImplementedException();
        }

        public Task<NewsList> FindLatest()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<NewsList>> FindRandom()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<NewsList>> FindTop()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<NewsList>> FindNewest()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<NewsList>> Findfeatured()
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<NewsList>> GetAllInCategory(string category)
        {
            throw new NotImplementedException();
        }
    }
}
