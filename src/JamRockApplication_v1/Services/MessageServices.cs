﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JamRockApplication_v1.Interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace JamRockApplication_v1.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private readonly ILogger<AuthMessageSender> _logger;
        private const string SendGridApiKey = "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";

        public AuthMessageSender(ILogger<AuthMessageSender> logger)
        {
            _logger = logger;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            try
            {

                var apiKey = SendGridApiKey;
                var client = new SendGridClient(apiKey);


                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("jamrock.ng@gmail.com", "Jamrock Shop"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email),
                    new EmailAddress("oyabiz@oyanow.ng")
                   
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);
            }
            catch (Exception e)
            {
                _logger.LogError("Error sending mail", e.ToString());
            }
            await Task.FromResult(0);
        }

        public async Task SendEmailSupportAsync(string email, string subject, string message)
        {
            try
            {

                const string apiKey = SendGridApiKey;
                var client = new SendGridClient(apiKey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress(email, "Jamrock Site Support"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress("jamrock.ng@gmail.com"),
                    new EmailAddress("support@heuristixs.com")
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public async Task SendEmailContactAsync(string email, string subject, string message)
        {
            try
            {

                const string apiKey = SendGridApiKey;
                var client = new SendGridClient(apiKey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress(email, "Jamrock Site"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress("jamrock.ng@gmail.com"),
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
