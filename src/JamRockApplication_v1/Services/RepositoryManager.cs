﻿using JamRockApplication_v1.Interfaces;
using Microsoft.AspNetCore.Http;
using System.IO;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;

namespace JamRockApplication_v1.Services
{
    public class RepositoryManager : IRepositoryManager
    {
        private IConfiguration Configuration { get; set; }
        private readonly ILogger<IRepositoryManager> _logger;


        private const string CdnPathEvents = "https://jamrock.azureedge.net/events/";
        private const string CdnPathProducts = "https://jamrock.azureedge.net/shop/";
        private const string CdnPathGallery = "https://jamrock.azureedge.net/gallery/";
        private const string CdnPathPost = "https://jamrock.azureedge.net/blog/";

        public RepositoryManager(IConfiguration configuration, ILogger<IRepositoryManager> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }
        public string UploadEventImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("events");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                return CdnPathEvents + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }

        public string UploadProductImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("shop");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                return CdnPathProducts + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }

        byte[] IRepositoryManager.GetByteArrayFromImage(IFormFile file)
        {
            if (file.Length > 0)
            {
                using (var fileStream = file.OpenReadStream())
                using (var ms = new MemoryStream())
                {
                    fileStream.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    return fileBytes;
                }
            }
            else
            {
                byte[] b = null;
                return b;
            }
        }

        public string UploadGalleryImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("gallery");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                return CdnPathGallery + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }

        public string UploadPostImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("blog");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                return CdnPathPost + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }
    }
}
