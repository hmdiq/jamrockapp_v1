﻿var Image;
var Images = [];

$(document).ready(function () 
{

    $(".button-collapse").sideNav();
    $('[data-toggle="tooltip"]').tooltip();
    $("#toggle").tooltip("show");

    $(".mdb-select").material_select();

    $(".navbar-collapse a").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    $("#mdb-lightbox-ui").load("/mdb-addons/mdb-lightbox-ui.html");

    $(".sticky").sticky({
        topSpacing: 90
        , zIndex: 2
        , stopper: "#stopper"
    });

    $("#eventDate").datepicker({
        autoclose: true
    });


});

function previewFile(fileInput) {
    if (fileInput.files && fileInput.files[0]) {
        var reader = new FileReader();
        reader.onloadend = function (e) {
            $("#imgPreview").attr("src", e.target.result);
        }

        reader.readAsDataURL(fileInput.files[0]);
    }
}

function EditpreviewFile(fileInput)
{
    if (fileInput.files && fileInput.files[0])
    {
        Image = fileInput.files[0];
        console.log(Image);
        var reader = new FileReader();
        reader.onloadend = function (e) {
            $("#imgPreviewEdit").attr("src", e.target.result);
        }

        reader.readAsDataURL(fileInput.files[0]);
    }
}

function AddToCart(id)
{
    $.ajax({
        url: "/Shop/AddToCart",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, choice: document.getElementById("cs").value },
        success: function (result) {
            toastr.success("Added Item To Cart!");
            toastr.options.closeButton = true;
        },
        error: function (error) {
            console.log(error);
            toastr.warning(error.responseJSON.error);
            toastr.options.closeButton = true;
        }
    });
}

function payWithPaystack() {
    key = document.getElementById("key").value;
    email = document.getElementById("customerEmail").value;
    amount = document.getElementById("totalCost").value * 100;
    ref = document.getElementById("reference").value;
    name = document.getElementById("customerName").value;
    address = document.getElementById("customerAddress").value;
    phone = document.getElementById("customerPhone").value;
    delivery = $('input[name="deliveryOption"]:checked').val();


    var processingFees = ((0.015) * amount + 10000);

    if (delivery === "sameDay")
    {
        amount = amount + 50000;
    }

    var handler = PaystackPop.setup({
        key: key,
        email: email,
        amount: amount + processingFees,
        ref: ref,
        callback: function (response)
        {
            var formData = new FormData();

            formData.append("email", email);
            formData.append("name", name);
            formData.append("phone", phone);
            formData.append("address", address);
            formData.append("refNumber", response.reference);
            formData.append("DeliveryMode", delivery);

            toastr.info("Give us a minute while we prepare your order receipt");
            toastr.options.closeButton = true;
            transactionDone(formData);
        },
        onClose: function () {
            toastr.warning("You Closed The Window!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;
        }
    });
    handler.openIframe();
}

function transactionDone(form)
{
    $.ajax({
        url: "/Shop/transcationComplete",
        type: "POST",
        processData: false,
        contentType: false,
        cache: false,
        data: form,
        success: function (result)
        {
            toastr.info("All done, check your inbox");
            toastr.options.closeButton = true;

            setTimeout(window.location = "/Shop/Index", 2000);


        },
        error: function (error) {
            toastr.error("Error Completing Sale");
            toastr.error("Contact Admin");
            toastr.options.timeOut = 2;
            toastr.options.closeButton = true;
        }
    });

}

function Increase(id) {
    $.ajax({
        url: "/Shop/UpdateItemQuantityPlus",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id },
        success: function (result) {
            //UpdateCartTable();
            window.location = "/Shop/Cart";
            toastr.success("Cart Updated!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;

            console.log(result);
        },
        error: function (error) {
            toastr.error("Error Updating Cart!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;
            console.log(error);
        }
    });

}

function Decrease(id) {
    $.ajax({
        url: "/Shop/UpdateItemQuantityMinus",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id },
        success: function (result) {
            //UpdateCartTable();

            window.location = "/Shop/Cart";

            toastr.success("Cart Updated!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;

            console.log(result);
        },
        error: function (error) {
            toastr.error("Error Updating Cart!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;
            console.log(error);
        }
    });

}

function Remove(id) {
    $.ajax({
        url: "/Shop/RemoveFromCart",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id },
        success: function (result) {
            //UpdateCartTable();

            window.location = "/Shop/Cart";

            toastr.info("Item Removed!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;

            console.log(result);
        },
        error: function (error) {
            toastr.error("Error Removing Item!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;
            console.log(error);
        }
    });

}

function UpdateCartTable() {
    $("#cartTableView").load("CartTable");
}

function setImages(fileInput)
{
    if (fileInput.files)
    {
        Images = fileInput.files;
    }
}

function startUpload(id) {
    var formData = new FormData();

    for (var i = 0; i < Images.length; i++) {
        var name = "file" + [i];
        formData.append(name, Images[i]);
    }

    formData.append("event", id);

    $.ajax({
        url: "/Event/AddImages",
        type: "POST",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
            toastr.success("Gallery Updated!");
            toastr.options.closeButton = true;
            console.log(result);
            window.location = "/Event/eventDetails/" + id;
        },
        error: function (error) {
            toastr.error("Error!");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function deleteSelected()
{
    var selectedData = new Array();

    $("#tb input[type=checkbox]")
        .each(function() {
            if (this.checked) {
                selectedData.push(this.id);
            }
        });

    $.ajax({
        url:"DeleteMultiple",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: {selected:selectedData}
    });
}

function updateStock(id) {
    var quantity = document.getElementById("newStock").value;
    $.ajax({
        url: "updateStock",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { Id: id, Quantity: quantity },
        success: function (result) {

            toastr.success("Stock Updated!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;

            console.log(result);
        },
        error: function (error) {
            toastr.error("Error Updating Stock!");
            toastr.options.timeOut = 10;
            toastr.options.extendedTimeOut = 10;
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function init_map() {

    var var_location = new google.maps.LatLng(9.0766198,7.4257242);

    var var_mapoptions = {
        center: var_location,
        zoom: 18
    };

    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "Secret Garden"
    });

    var var_map = new google.maps.Map(document.getElementById("map-container"),
        var_mapoptions);

    var_marker.setMap(var_map);

}

function sendMail()
{
    var data =
           {
               Name: document.getElementById("emailName").value,
               Email: document.getElementById("emailAddress").value,
               Subject: document.getElementById("emailSubject").value,
               Content:  document.getElementById("emailContent").value
           }

    $.ajax({
        url: "/Home/sendMail",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { model: data },
        success: function(result) {
            toastr.success("Mail Sent!");
            toastr.options.closeButton = true;
        },
        error: function(error) {
            toastr.error("Error!");
            toastr.options.closeButton = true;

        }
    });
}

function updateProductName(Id)
{
    console.log("Updating name");
    var newName = document.getElementById("productName").value;

    $.ajax({
        url: "/Product/UpdateProductName",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, name: newName },
        success: function (result)
        {
            window.location = "/Product/updateProduct/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateProductQuantity(Id) {
    var newQuantity = document.getElementById("productQuantity").value;

    $.ajax({
        url: "/Product/UpdateProductQuantity",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, quantity: newQuantity },
        success: function (result) {
            window.location = "/Product/updateProduct/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateProductDescription(Id) {
    var newDescription = document.getElementById("productDescription").value;

    $.ajax({
        url: "/Product/UpdateProductDescription",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, Description: newDescription },
        success: function (result) {
            window.location = "/Product/updateProduct/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateProductCategory(Id) {
    var newCategory = document.getElementById("productCategory").value;

    $.ajax({
        url: "/Product/UpdateProductCategory",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, Category: newCategory },
        success: function (result) {
            window.location = "/Product/updateProduct/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateProductGender(Id) {
    var newGender = document.getElementById("productGender").value;

    $.ajax({
        url: "/Product/UpdateProductGender",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, Gender: newGender },
        success: function (result) {
            window.location = "/Product/updateProduct/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateEventName(Id) {

    var newName = document.getElementById("eventName").value;

    $.ajax({
        url: "/Event/UpdateEventName",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, name: newName },
        success: function (result) {
            window.location = "/Event/eventDetails/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateEventLocation(Id) {

    var newLocation = document.getElementById("eventLocation").value;

    $.ajax({
        url: "/Event/UpdateEventLocation",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: Id, Location: newLocation },
        success: function (result) {
            window.location = "/Event/eventDetails/" + Id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateEventDescription(id) {

    var newDescription = document.getElementById("eventDescription").value;

    $.ajax({
        url: "/Event/UpdateEventDescription",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, Description: newDescription },
        success: function (result) {
            window.location = "/Event/eventDetails/" + id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateEventDate(id) {

    var newDate = document.getElementById("eventDate").value;

    $.ajax({
        url: "/Event/UpdateEventDate",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, Date: newDate },
        success: function (result) {
            window.location = "/Event/eventDetails/" + id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function addNewColorSize(id)
{
    var data =
    {
        Color: document.getElementById("color").value,
        Size: document.getElementById("size").value,
        Quantity: document.getElementById("quantity").value
    }

    $.ajax({
        url: "/Product/AddColorSize",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { id: id, model: data },
        success: function(result) {
            toastr.success("Added");
            toastr.options.closeButton = true;

            window.location = "/Product/UpdateProduct/" + id;
        },
        error: function(error) {
            toastr.error("Error!");
            toastr.options.closeButton = true;

        }
    });
}

function updateProductImage(id)
{
    console.log(id);
    console.log(Image);

    var formData = new FormData();

    formData.append("id", id);
    formData.append("image", Image);

    $.ajax({
        url: "/Product/UpdateProductImage",
        type: "POST",
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result)
        {
            toastr.success("Image Updated!");
            toastr.options.closeButton = true;
            console.log(result);
            window.location = "/Product/updateProduct/" + id;
        },
        error: function (error)
        {
            toastr.error("Error!");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateEventImage(id)
{
    var formData = new FormData();

    formData.append("id", id);
    formData.append("image", Image);

    $.ajax({
        url: "/Event/UpdateEventImage",
        type: "POST",
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
            toastr.success("Image Updated!");
            toastr.options.closeButton = true;
            console.log(result);
            window.location = "/Event/eventDetails/" + Id;
        },
        error: function (error) {
            toastr.error("Error!");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateBlogImage(id) {

    var formData = new FormData();

    formData.append("id", id);
    formData.append("image", Image);

    $.ajax({
        url: "/Blog/UpdateBlogImage",
        type: "POST",
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
            toastr.success("Image Updated!");
            toastr.options.closeButton = true;
            console.log(result);
            window.location = "/Blog/Edit/" + Id;
        },
        error: function (error) {
            toastr.error("Error!");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateBlogContent(id) {

    var content = document.getElementById("blogContent").value;

    $.ajax({
        url: "/Blog/UpdateBlogContent",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, content: content },
        success: function (result) {
            window.location = "/Blog/Edit/" + id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateBlogTitle(id) {

    var title = document.getElementById("blogTitle").value;

    $.ajax({
        url: "/Blog/UpdateBlogTitle",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, title: title },
        success: function (result) {
            window.location = "/Blog/Edit/" + id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}

function updateBlogCategory(id) {

    var category = document.getElementById("blogCategory").value;

    $.ajax({
        url: "/Blog/UpdateBlogCategory",
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data: { ID: id, category: category },
        success: function (result) {
            window.location = "/Blog/Edit/" + id;
        },
        error: function (error) {
            toastr.error("Error Updating");
            toastr.options.closeButton = true;
            console.log(error);
        }
    });
}