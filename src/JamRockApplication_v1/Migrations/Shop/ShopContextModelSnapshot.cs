﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using JamRockApplication_v1.Data;

namespace JamRockApplication_v1.Migrations.Shop
{
    [DbContext(typeof(ShopContext))]
    partial class ShopContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Artists", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("RaveId");

                    b.Property<string>("avatar");

                    b.HasKey("Id");

                    b.HasIndex("RaveId");

                    b.ToTable("Artists");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Cart", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("Quantity");

                    b.Property<DateTime>("createdOn");

                    b.Property<DateTime>("lastModified");

                    b.Property<double>("totalCost");

                    b.HasKey("Id");

                    b.ToTable("Cart");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.cartItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Amount");

                    b.Property<string>("CartId");

                    b.Property<int?>("OrderId");

                    b.Property<string>("ProductColor");

                    b.Property<string>("ProductImage");

                    b.Property<string>("ProductName");

                    b.Property<double>("ProductPrice");

                    b.Property<int>("ProductQuantity");

                    b.Property<string>("ProductSize");

                    b.Property<int>("ProductSku");

                    b.HasKey("Id");

                    b.HasIndex("CartId");

                    b.HasIndex("OrderId");

                    b.ToTable("cartItem");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.ColorSize", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<int?>("ProductID");

                    b.Property<int>("Quantity");

                    b.Property<int>("QuantityLeft");

                    b.Property<string>("Size");

                    b.HasKey("Id");

                    b.HasIndex("ProductID");

                    b.ToTable("ColorSize");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.galleryItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Image");

                    b.Property<int?>("RaveID");

                    b.HasKey("Id");

                    b.HasIndex("RaveID");

                    b.ToTable("galleryItem");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CustomerId");

                    b.Property<string>("DeliveryType");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<DateTime>("LastModified");

                    b.Property<string>("content");

                    b.Property<string>("newsImage");

                    b.Property<string>("title");

                    b.HasKey("Id");

                    b.ToTable("Post");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Product", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category");

                    b.Property<string>("Description");

                    b.Property<string>("Gender");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.Property<int>("Quantity");

                    b.Property<int>("QuantitySold");

                    b.Property<DateTime>("createdOn");

                    b.Property<string>("productImageByteArray");

                    b.HasKey("ID");

                    b.ToTable("Product");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Rave", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<DateTime>("LastModified");

                    b.Property<string>("Location");

                    b.Property<string>("Name");

                    b.Property<DateTime>("eventDate");

                    b.Property<string>("eventImage");

                    b.HasKey("ID");

                    b.ToTable("Rave");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.NewsList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("NewsList");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Artists", b =>
                {
                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Rave", "Rave")
                        .WithMany("Artists")
                        .HasForeignKey("RaveId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.cartItem", b =>
                {
                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Cart", "Cart")
                        .WithMany("cartItems")
                        .HasForeignKey("CartId");

                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Order")
                        .WithMany("OrderItems")
                        .HasForeignKey("OrderId");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.ColorSize", b =>
                {
                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Product")
                        .WithMany("ColorSizes")
                        .HasForeignKey("ProductID");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.galleryItem", b =>
                {
                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Rave")
                        .WithMany("Gallery")
                        .HasForeignKey("RaveID");
                });

            modelBuilder.Entity("JamRockApplication_v1.Models.AdminModels.Order", b =>
                {
                    b.HasOne("JamRockApplication_v1.Models.AdminModels.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
