﻿using System.Threading.Tasks;

namespace JamRockApplication_v1.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendEmailSupportAsync(string email, string subject, string message);

        Task SendEmailContactAsync(string email, string subject, string message);
    }
}
