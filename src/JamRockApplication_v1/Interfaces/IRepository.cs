﻿using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IQueryable<T>> GetAll();
        Task Add(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task<T> FindById(int? Id);
        Task<T> FindById(string Id);
        Task<T> FindLast();
        Task<T> FindLatest();
        Task<IQueryable<T>> FindRandom();
        Task<IQueryable<T>> FindTop();
        Task<IQueryable<T>> FindNewest();
        Task<IQueryable<T>> Findfeatured();
        Task<IQueryable<T>> GetAllInCategory(string category);
    }
}
