﻿using System.Threading.Tasks;

namespace JamRockApplication_v1.Interfaces
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
