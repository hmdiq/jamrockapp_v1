﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JamRockApplication_v1.Interfaces
{
    public interface IUserManager<in T> where T : class
    {
        Task<bool> Registration(T entity);
    }
}
