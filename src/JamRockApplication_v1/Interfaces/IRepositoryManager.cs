﻿using Microsoft.AspNetCore.Http;

namespace JamRockApplication_v1.Interfaces
{
    public interface IRepositoryManager
    {
        byte[] GetByteArrayFromImage(IFormFile file);

        string UploadEventImageToCdn(IFormFile imageFile);

        string UploadProductImageToCdn(IFormFile imageFile);

        string UploadGalleryImageToCdn(IFormFile imageFile);

        string UploadPostImageToCdn(IFormFile imageFile);
    }
}
