﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using JamRockApplication_v1.Data;
using JamRockApplication_v1.Models;
using JamRockApplication_v1.Services;
using Serilog;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using JamRockApplication_v1.Interfaces;
using JamRockApplication_v1.Models.AccountViewModels;
using JamRockApplication_v1.Models.AdminModels;
using JamRockApplication_v1.Services.AdminServices;
using JamRockApplication_v1.Services.ShopServices;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Extensions.FileProviders;
using NetEscapades.AspNetCore.SecurityHeaders;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using AuthenticationContext = Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext;

namespace JamRockApplication_v1
{
    public class Startup
    {
        public ActiveDirectoryClient AadClient { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                 .MinimumLevel.Debug()
                 .WriteTo.RollingFile(Path.Combine(
                     env.ContentRootPath, "log-{Date}.txt"))
                 .CreateLogger();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddDbContext<ShopContext>(options =>
                 options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            services.AddOptions();
            services.AddCustomHeaders();
            services.AddScoped(x => x
                .GetRequiredService<IUrlHelperFactory>()
                .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext));

            services.AddRouting(
                options =>
                {
                    // Improve SEO by stopping duplicate URL's due to case differences or trailing slashes.
                    // See http://googlewebmastercentral.blogspot.co.uk/2010/04/to-slash-or-not-to-slash.html
                    // All generated URL's should append a trailing slash.
                    options.AppendTrailingSlash = true;
                    // All generated URL's should be lower-case.
                    options.LowercaseUrls = true;
                });

            services.AddResponseCaching();

            services.AddMvc().AddMvcOptions(options => options.RespectBrowserAcceptHeader = true)
                   .AddXmlSerializerFormatters()
                   .AddJsonOptions(config =>
                   {
                       config.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                   });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(1);
                options.CookieHttpOnly = true;
            });

            services.AddAuthentication(
                sharedOptions => sharedOptions.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddTransient<IRepository<Product>, ProductRepo>();
            services.AddTransient<IRepository<Rave>, RaveRepo>();
            services.AddTransient<IRepository<Cart>, CartRepo>();
            services.AddTransient<IRepository<Post>, BlogRepo>();
            services.AddTransient<IRepository<Customer>, CustomerRepo>();
            services.AddTransient<IRepository<Order>,OrderRepo>();
            services.AddTransient<IRepository<NewsList>, NewsLetterService>();
            services.AddTransient<IUserManager<RegisterViewModel>, UserService>();

            services.AddTransient<IRepositoryManager, RepositoryManager>();
            services.AddSingleton<IConfiguration>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddDebug(Microsoft.Extensions.Logging.LogLevel.Information).AddSerilog();
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                var options = new RewriteOptions()
                    .AddRedirectToHttpsPermanent();

                app.UseRewriter(options);
                app.UseRewriter(new RewriteOptions().Add(new RedirectWwwRule()));

                var policyCollection = new HeaderPolicyCollection()
                    .AddStrictTransportSecurityMaxAge(maxAge: 60 * 60 * 24 * 365);
                app.UseCustomHeadersMiddleware(policyCollection);
            }


            app.UseStaticFiles();

            var rootPath = Path.GetFullPath(".");
            var acmeChallengePath =
                Path.Combine(rootPath, @".well-known\acme-challenge");

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(acmeChallengePath),
                RequestPath = new PathString("/.well-known/acme-challenge"),
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true
            });

            //app.UseIdentity();
            app.UseSession();

            app.UseCookieAuthentication(new CookieAuthenticationOptions());


            var authContext = new AuthenticationContext("https://login.microsoftonline.com/jamrockng.onmicrosoft.com");
            var clientCredential = new ClientCredential("8a6d282b-663a-45f7-8033-3d480994d851", "aYo+Y6Ts65CsBNtqFgREXJ53CY8Gc35d44nOQxekiTE="); //the azure app directory appid and secret key
            const string aadGraphUri = "https://graph.windows.net";
            var graphUri = new Uri(aadGraphUri);
            var serviceRoot = new Uri(graphUri, "jamrockng.onmicrosoft.com");

            AadClient = new ActiveDirectoryClient(serviceRoot, async () => await AcquireGraphApiAccessToken(aadGraphUri, authContext, clientCredential));

            app.UseOpenIdConnectAuthentication(new OpenIdConnectOptions()
            {
                Authority = "https://login.microsoftonline.com/tfp" + "/jamrockng.onmicrosoft.com" + "/B2C_1_jamrock-log-in" + "/v2.0", //the tenant name + the policy name + version number
                ClientId = "6f1c9710-d6e2-4d35-baa3-62273526710d", //the id of the b2c application
                ClientSecret = "81}l\\ASBiUu1!37x",//the secret of the b2c applcation
                ResponseType = OpenIdConnectResponseType.IdToken,
                UseTokenLifetime = true,
                PostLogoutRedirectUri = "https://www.jamrocknigeria.com",
                Events = new OpenIdConnectEvents()
                {
                    OnTokenValidated = SecurityTokenValidated,
                    OnRemoteFailure = OnRemoteFailure
                },
                CallbackPath = new PathString("/signin-oidc") //the authentication type which is oidc in this case
            });


            app.UseResponseCaching();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public Task OnRemoteFailure(FailureContext context)
        {
            context.HandleResponse();
            // Handle the error code that Azure AD B2C throws when trying to reset a password from the login page 
            // because password reset is not supported by a "sign-up or sign-in policy"
            if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("AADB2C90118"))
            {
                // If the user clicked the reset password link, redirect to the reset password route
                context.Response.Redirect("/Session/ResetPassword");
            }
            else if (context.Failure is OpenIdConnectProtocolException && context.Failure.Message.Contains("access_denied"))
            {
                context.Response.Redirect("/");
            }
            else
            {
                Console.Write(context.Failure);
                context.Response.Redirect("/Home/Error?message=" + context.Failure.Message);
            }
            return Task.FromResult(0);
        }

        private static async Task<string> AcquireGraphApiAccessToken(string graphApiUrl, AuthenticationContext authContext, ClientCredential clientCredential)
        {
            AuthenticationResult result = null;
            var retryCount = 0;
            bool retry;
            do
            {
                retry = false;
                try
                {
                    result = await authContext.AcquireTokenAsync(graphApiUrl, clientCredential);

                }
                catch (AdalException ex)
                {
                    if (ex.ErrorCode != "temporarily_unavailable") continue;
                    retry = true;
                    retryCount++;
                    await Task.Delay(3000);
                }
            } while (retry && (retryCount < 3));

            return result?.AccessToken;
        }

        private Task SecurityTokenValidated(TokenValidatedContext context)
        {
            return Task.Run(async () =>
            {
                var oidClaim = context.SecurityToken.Claims.FirstOrDefault(c => c.Type == "oid");

                if (!string.IsNullOrWhiteSpace(oidClaim?.Value))
                {
                    var pagedCollection = await AadClient.Users.GetByObjectId(oidClaim.Value).MemberOf.ExecuteAsync();

                    do
                    {
                        var directoryObjects = pagedCollection.CurrentPage.ToList();
                        foreach (var directoryObject in directoryObjects)
                        {
                            var group = directoryObject as Group;

                            if (group != null)
                            {
                                ((ClaimsIdentity)context.Ticket.Principal.Identity).AddClaim(new Claim(ClaimTypes.Role, group.DisplayName, ClaimValueTypes.String));
                            }
                        }
                        pagedCollection = pagedCollection.MorePagesAvailable ? await pagedCollection.GetNextPageAsync() : null;
                    }
                    while (pagedCollection != null);
                }
            });
        }
    }
}
