﻿using JamRockApplication_v1.Models;
using JamRockApplication_v1.Models.AdminModels;
using Microsoft.EntityFrameworkCore;

namespace JamRockApplication_v1.Data
{
    public class ShopContext : DbContext
    {
        public ShopContext(DbContextOptions options) : base(options)
        {
        }

        public ShopContext()
        {
            //Database.Migrate();
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Rave> Events { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Post> Posts { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<NewsList> NewsLists { get; set; }

        //create the db
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<Rave>().ToTable("Rave");
            modelBuilder.Entity<Cart>().ToTable("Cart");
            modelBuilder.Entity<Post>().ToTable("Post");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<NewsList>().ToTable("NewsList");
        }

    }
}
